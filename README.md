![输入图片说明](doc/%E8%AE%BE%E7%BD%AE%E5%AE%A1%E6%89%B9%E4%BA%BA.png)
有兴趣请帮点一下Star，谢谢支持。


1、本项目为JFinalOA项目，使用jfinal开发、spring版本为点狮项目。

<br/>
2、JFinalOA为全开源版本。点狮完整源代码请联系管理员。
<br/>
3、点狮使用若依前后端分离版本开发
<br/>
4、点狮各个模块体验地址


### 1、点狮系统体验地址：<a href="http://admin.dianshixinxi.com:90" target="_blank">admin.dianshixinxi.com:90</a>

用户密码已填好，可直接登录
<br/>
<br/>
备注：因为演示站使用物理机+DDNS方式部署，因为公网ip会改变，如果访问出现网络无法访问的情况，可打开cmd命令行窗口执行  ipconfig /flushdns  命令，清空本地DNS缓存
<br/>
用户密码已填好，可直接登录
<br/>
<br/>

5、JFinalOA体验地址：<a href="http://admin.dianshixinxi.com:8081/JFinalOA" target="_blank">admin.dianshixinxi.com:8081/JFinalOA</a>
<br/>
默认密码：admin/admin
<br/>
其他用户的帐号密码：帐号密码均为姓名的全拼。



以下站点只提供部分后台代码

点狮OA

**https://gitee.com/glorylion/pointlion-oa** 

点狮HRM

 **https://gitee.com/glorylion/pointlion-hrm** 

点狮CRM

 **https://gitee.com/glorylion/pointlion-crm** 

点狮AM（档案）

 **https://gitee.com/glorylion/pointlion-am** 





 **点狮-项目特点** 
<br/>
1、点狮OA是一套【多租户】的企业办公系统，可以提供给【集团级企业用户】使用，也可对外提供未SAAS服务多公司进行入驻。【租户】可以设置自己公司独有的【个性化流程】【人员管理】【用户管理】【角色管理】【菜单权限】【所有的单据申请信息】等等。<br/>
2、点狮OA基于【点狮后台管理】开发，该平台可以无缝扩展集成现有其他项目【点狮HRM】、【点狮AM（档案）】、【点狮CRM】、【点狮IM（即时通讯）】、【点狮ERP（企业资源管理）】等等。以及可以作为【点狮OA-APP】和【小程序】的后台服务。<br/>
3、点狮OA扩展了流程设计器，可以手动设置指定任务的办理人，以及指定岗位办理，流程设计更加方便。<br/>
4、点狮OA集成了Flowable流程引擎，可以实现，流程审批的，【并行】、【串行】、【会签】、【回退】、【取回】，等操作。<br/>
5、系统内置，任务办理的【转办】【委托】【抄送】等功能。<br/>
<br/>





### JFinalOA项目介绍

<a href="https://blog.csdn.net/qq_30739519/article/details/82493456" target="_blank">
    最新版已从Activiti升级为Flowable（点击查看Activiti与Flowable区别）
</a>


<br/><br/>


 **（QQ群满了，大家有问题加微信吧，个人微信号：yigexiaochengxuyuan  QQ:439635374）** 



演示项目中有很多审批流的业务为自己开发使用仅供参考（不方便开源），开源项目中仅提供公章使用申请作为示例代码，其他基础功能全部开源，望知晓。



点狮详情咨询：


<img src="doc/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20230906105917.jpg" width="300px">



1. JDK请使用1.8（flowable要求）
2. 数据库如果导入失败，请使用mysql5.6+navicat还原psc文件
3. 启动项目如果出现一套大写的ACT开头的数据库表，请将大写表删除之后，设置数据库忽略大小写，重新启动项目


**1.简单介绍** 

项目主要提供办公系统的开发人员提供一套带有基本系统管理以及流程管理的开发平台，为办公常用申请部分解决方案。可快速开发办公常用的各类流程功能。

选用的技术框架基本都是简单易上手的，方便自己用来做办公类项目，以及其他类型小项目都会比较方便。

**2.如何运行** 
1. 克隆项目之后，导入项目到eclipse。
2. 执行maven update project下载jar包。
3. 配置maven run config使用命令tomcat7:run运行。
4. 或者添加tomcat server将项目部署到tomcat运行。


### 以下为点狮OA部分功能截图

![输入图片说明](doc/%E5%8D%B3%E6%97%B6%E9%80%9A%E8%AE%AF.png)
![输入图片说明](doc/%E5%8A%9E%E7%90%86%E4%BB%BB%E5%8A%A1.png)
![输入图片说明](doc/%E6%8A%A5%E9%94%80%E7%94%B3%E8%AF%B7.png)
![输入图片说明](doc/%E5%BE%85%E5%8A%9E.png)
![输入图片说明](doc/%E6%B5%81%E7%A8%8B%E5%9B%BE%E8%AE%BE%E8%AE%A1%E5%99%A8.png)
![输入图片说明](doc/%E6%B5%81%E7%A8%8B%E4%BF%A1%E6%81%AF%E5%B1%95%E7%A4%BA.png)
![输入图片说明](doc/%E8%87%AA%E5%AE%9A%E4%B9%89%E8%A1%A8%E5%8D%95%E8%AE%BE%E8%AE%A1%E5%99%A8.png)
![输入图片说明](doc/%E8%87%AA%E5%AE%9A%E4%B9%89%E8%A1%A8%E5%8D%95%E7%94%B3%E8%AF%B7.png)


###  以下为JFinalOA部分截图

登录
![输入图片说明](https://images.gitee.com/uploads/images/2019/0114/095251_f70c0376_868436.png "在这里输入图片标题")
即时通讯
![输入图片说明](https://images.gitee.com/uploads/images/2019/0114/095251_9761cd0e_868436.png "在这里输入图片标题")
UI
![输入图片说明](https://images.gitee.com/uploads/images/2019/0114/095251_89761846_868436.png "在这里输入图片标题")
公文DEMO
![输入图片说明](https://images.gitee.com/uploads/images/2019/0114/095251_fce70eef_868436.png "在这里输入图片标题")
流转历史
![输入图片说明](https://images.gitee.com/uploads/images/2019/0114/095251_af8a034f_868436.png "在这里输入图片标题")
流程跟踪
![输入图片说明](https://images.gitee.com/uploads/images/2019/0114/095252_fcce3784_868436.png "在这里输入图片标题")
菜单管理
![输入图片说明](https://images.gitee.com/uploads/images/2019/0114/095252_3d7d1e64_868436.png "在这里输入图片标题")
用户管理
![输入图片说明](https://images.gitee.com/uploads/images/2019/0114/095252_34434dc4_868436.png "在这里输入图片标题")
流程管理
![输入图片说明](https://images.gitee.com/uploads/images/2019/0114/095252_763b9834_868436.png "在这里输入图片标题")
流程编辑
![输入图片说明](https://images.gitee.com/uploads/images/2019/0114/095252_c1cba53c_868436.png "在这里输入图片标题")
代码生成
![输入图片说明](https://images.gitee.com/uploads/images/2019/0114/095252_5ce7189b_868436.png "在这里输入图片标题")

<br/><br/><br/><br/>
 



