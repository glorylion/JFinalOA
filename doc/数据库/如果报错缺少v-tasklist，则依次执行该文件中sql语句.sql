CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_candidate_candidate` AS select `i`.`TASK_ID_` AS `TASK_ID_`,`i`.`USER_ID_` AS `USER_ID` from (`act_ru_identitylink` `i` join `act_ru_task` `t`) where ((`i`.`TASK_ID_` is not null) and (`i`.`USER_ID_` is not null) and (`i`.`TASK_ID_` = `t`.`ID_`) and (`t`.`ASSIGNEE_` is null) and (`i`.`TYPE_` = 'candidate'));



CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_candidate_group` AS select `i`.`TASK_ID_` AS `TASK_ID_`,`m`.`USER_ID_` AS `USER_ID` from ((`act_ru_identitylink` `i` join `act_ru_task` `t`) join `act_id_membership` `m`) where ((`i`.`TASK_ID_` = `t`.`ID_`) and (`i`.`TASK_ID_` is not null) and (`i`.`USER_ID_` is null) and (`t`.`ASSIGNEE_` is null) and (`i`.`TYPE_` = 'candidate') and (`m`.`GROUP_ID_` = `i`.`GROUP_ID_`));



CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_candidate_z_distinct` AS select `v_candidate_candidate`.`TASK_ID_` AS `TASK_ID_`,`v_candidate_candidate`.`USER_ID` AS `USER_ID` from `v_candidate_candidate` union select `v_candidate_group`.`TASK_ID_` AS `TASK_ID_`,`v_candidate_group`.`USER_ID` AS `USER_ID` from `v_candidate_group`;




CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_tasklist` AS select `a`.`ID_` AS `TASKID`,`a`.`PROC_INST_ID_` AS `INSID`,`a`.`TASK_DEF_KEY_` AS `TASKDEFKEY`,`d`.`KEY_` AS `DEFKEY`,`d`.`NAME_` AS `DEFNAME`,`a`.`NAME_` AS `TASKNAME`,`a`.`ASSIGNEE_` AS `ASSIGNEE`,`i`.`USER_ID` AS `CANDIDATE`,`a`.`PROC_DEF_ID_` AS `DEFID`,`a`.`DELEGATION_` AS `DELEGATIONID`,`a`.`DESCRIPTION_` AS `DESCRIPTION`,`v`.`TEXT_` AS `ORDERINFO`,substring_index(`p`.`BUSINESS_KEY_`,':',1) AS `BILLTYPE`,substring_index(`p`.`BUSINESS_KEY_`,':',-(1)) AS `BUSINESSID`,date_format(`a`.`CREATE_TIME_`,'%Y-%m-%d %H:%i:%s') AS `CREATETIME`,date_format(`a`.`DUE_DATE_`,'%Y-%m-%d %H:%i:%s') AS `DUEDATE` from ((((`act_ru_task` `a` left join `v_candidate_z_distinct` `i` on((`a`.`ID_` = `i`.`TASK_ID_`))) left join `act_re_procdef` `d` on((`a`.`PROC_DEF_ID_` = `d`.`ID_`))) left join `act_ru_variable` `v` on(((`a`.`PROC_INST_ID_` = `v`.`PROC_INST_ID_`) and (`v`.`NAME_` = 'orderinfo')))) left join `act_hi_procinst` `p` on((`a`.`PROC_INST_ID_` = `p`.`ID_`)));